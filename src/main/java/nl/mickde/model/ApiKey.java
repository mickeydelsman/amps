package nl.mickde.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ApiKey {

    @Id
    private String key;

    public ApiKey() { }

    public ApiKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
