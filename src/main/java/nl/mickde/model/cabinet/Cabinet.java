package nl.mickde.model.cabinet;

import javax.persistence.*;

@Entity
public class Cabinet {

    @Id
    @SequenceGenerator(name="cabinet_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cabinet_seq")
    private long id;
    private double heigth, width;

    public Cabinet() { }

    public Cabinet(double height, double width) {
        this.heigth = height;
        this.width = width;
    }

    public long getId() {
        return id;
    }

    public double getHeigth() {
        return heigth;
    }

    public void setHeigth(double heigth) {
        this.heigth = heigth;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Cabinet{" +
                "id=" + id +
                ", heigth=" + heigth +
                ", width=" + width +
                '}';
    }
}