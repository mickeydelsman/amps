package nl.mickde.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class Item {

    @Id
    @GeneratedValue
    protected long id;
    protected int price;
    protected String name, brand;

    public Item() {}

    public Item(int price, String name, String brand) {
        this.price = price;
        this.name = name;
        this.brand = brand;
    }

    public long getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
