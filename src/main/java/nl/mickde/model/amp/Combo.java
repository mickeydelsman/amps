package nl.mickde.model.amp;

import nl.mickde.model.cabinet.Cabinet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Combo extends Amp {

    @OneToOne(cascade = {CascadeType.ALL})
    private Cabinet cabinet;

    // used for ...
    public Combo() { }

    // used for ...
    public Combo(String line) {
        super(line);
        cabinet = new Cabinet(17.5,24.5);
    }

    public Cabinet getCabinet() {
        return cabinet;
    }

    public void setCabinet(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    @Override
    public String toString() {
        return "Combo{" +
                "cabinet=" + cabinet +
                ", instrument='" + instrument + '\'' +
                ", type='" + type + '\'' +
                ", wattage=" + wattage +
                ", id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
