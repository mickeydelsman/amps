package nl.mickde.model.amp;

import nl.mickde.model.Item;
import javax.persistence.Entity;

@Entity
public abstract class Amp extends Item {

    protected String instrument, type;
    protected int wattage;

    // used for ...
    public Amp() { }

    // used for ...
    public Amp(String line) {
        this (
                Integer.parseInt(line.split(",")[1]),
                line.split(",")[2],
                line.split(",")[3],
                Integer.parseInt(line.split(",")[4]),
                line.split(",")[5],
                line.split(",")[0]
        );
    }

    public Amp(int price, String name, String brand, int wattage, String instrument, String type) {
        super(price, name, brand);
        this.wattage = wattage;
        this.instrument = instrument;
        this.type = type;
    }

    /*
    private String createNewId(String one, String two, String three) {

        Random r = new Random();
        // clear out current id
        id = "";

        List<String> strings = new ArrayList<>(Arrays.asList(one, two, three));

        strings.stream()
                .forEach(element -> {
                    if (!element.isEmpty()) {
                        id += (element.charAt(
                                r.nextInt(element.length())));
                    } id += r.nextInt(10);
                });

        return id;
    }
    */

    public int getWattage() {
        return wattage;
    }

    public void setWattage(int wattage) {
        this.wattage = wattage;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
