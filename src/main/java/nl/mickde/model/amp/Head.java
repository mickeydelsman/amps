package nl.mickde.model.amp;

import javax.persistence.Entity;

@Entity
public class Head extends Amp {

    // used for ...
    public Head() { }

    // used for ...
    public Head(String line) {
        super(line);
    }

    public Head(int price, String name, String brand, int wattage, String instrument, String type) {
        super(price, name, brand, wattage, instrument, type);
    }

    @Override
    public String toString() {
        return "Head{" +
                "instrument='" + instrument + '\'' +
                ", type='" + type + '\'' +
                ", wattage=" + wattage +
                ", id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
