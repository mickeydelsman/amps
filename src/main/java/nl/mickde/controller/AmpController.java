package nl.mickde.controller;

import nl.mickde.model.amp.Amp;
import nl.mickde.model.amp.Combo;
import nl.mickde.model.amp.Head;
import nl.mickde.service.AmpService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/amps")
public class AmpController {

    private AmpService service;

    // constructor used by spring to create controller object (object is managed by spring IoC container)
    public AmpController(AmpService service) {
        this.service = service;
    }

    // example of a basic GET request
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Amp> getAllAmps() {
        return service.getAllAmps();
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Amp> getAmpById(@PathVariable long id) {
        return service.getAmpById(id);
    }

    @GetMapping(value = "brand/{brand}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Amp> getAmpsByBrand(@PathVariable String brand) {
        return service.getAmpsByBrand(brand);
    }

    @GetMapping(value = "instrument/{instrument}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Amp> getAmpsByInstrument(@PathVariable String instrument) {
        return service.getAmpsByInstrument(instrument);
    }

    @GetMapping(value = "type/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Amp> getAmpsByType(@PathVariable String type) {
        return service.getAmpsByType(type);
    }

    // example of a basic POST request
    @PostMapping(value = "type/head/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addHead(@RequestBody Head head) {
        service.addHead(head);
    }

    @PostMapping(value = "type/combo/", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addCombo(@RequestBody Combo combo) {
        service.addCombo(combo);
    }
}
