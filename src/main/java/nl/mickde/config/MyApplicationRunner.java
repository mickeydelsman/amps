package nl.mickde.config;

import nl.mickde.model.ApiKey;
import nl.mickde.model.amp.Combo;
import nl.mickde.model.amp.Head;
import nl.mickde.repository.AmpRepository;
import nl.mickde.repository.ApiKeyRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class MyApplicationRunner implements ApplicationRunner {

    private AmpRepository ampRepository;
    private ApiKeyRepository apiKeyRepository;

    //
    public MyApplicationRunner(AmpRepository ampRepository, ApiKeyRepository apiKeyRepository) {

        this.ampRepository = ampRepository;
        this.apiKeyRepository = apiKeyRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Path path = Paths.get("src/main/resources/amps.csv");

        Files.lines(path)
                .forEach(line -> {
                    if (line.contains("Head")) ampRepository.save(new Head(line));
                    else if (line.contains("Combo")) ampRepository.save(new Combo(line));
                });

        List<ApiKey> apiKeyList = new ArrayList<>(
                Arrays.asList(
                        new ApiKey("c580a88f-f5de-4a69-9c10-809c09106a76"),
                        new ApiKey("cbc9d1d8-920f-4eb6-81b5-92a005667549"),
                        new ApiKey("728f5073-dd90-417c-9593-ff67f73fdcc2")
                )
        );

        for (ApiKey key : apiKeyList) {
            apiKeyRepository.save(key);
        }
    }
}