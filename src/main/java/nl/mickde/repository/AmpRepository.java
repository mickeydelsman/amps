package nl.mickde.repository;

import nl.mickde.model.amp.Amp;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AmpRepository extends CrudRepository<Amp, Long> {

    // custom query used by the amp service to get all amps by brand
    @Query("SELECT a FROM Amp a WHERE a.brand = ?1")
    Iterable<Amp> getAmpsByBrand(String brand);

    // custom method made up of generic operations which internally translates to a query (Get...By etc.)
    Iterable<Amp> getAllByType(String type);
}
