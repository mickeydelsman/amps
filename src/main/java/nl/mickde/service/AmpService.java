package nl.mickde.service;

import nl.mickde.model.amp.Amp;
import nl.mickde.model.amp.Combo;
import nl.mickde.model.amp.Head;
import nl.mickde.repository.AmpRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AmpService {

    private AmpRepository ampRepository;

    /* example of old way of storing data ->
    private List<Amp> amps = new ArrayList<>(
        Arrays.asList(
                new Amp("AC-15", "Guitar", "Vox"),
                new Amp("Bassman", "Bass", "Fender"),
                new Amp("Jazz Chorus", "Guitar", "Roland")
        )
    ); */

    public AmpService(AmpRepository ampRepository) {
        this.ampRepository = ampRepository;
    }

    public Iterable<Amp> getAllAmps() {
        return ampRepository.findAll();
    }

    public Iterable<Amp> getAmpsByBrand(String brand) {
        return ampRepository.getAmpsByBrand(StringUtils.capitalize(brand));
    }

    // example of old way of getting data and filtering it manually
    public Iterable<Amp> getAmpsByInstrument(String instrument) {

        List<Amp> list = (List<Amp>) ampRepository.findAll();

        list = list.stream()
                .filter(amp -> amp.getInstrument().toLowerCase().equals(instrument))
                .collect(Collectors.toList());

        return list;
    }

    // Question: could not return a normal Amp object, what are the consequences of using a optional?
    public Optional<Amp> getAmpById(long id) {
        return ampRepository.findById(id);
    }

    public Iterable<Amp> getAmpsByType(String type) {
        return ampRepository.getAllByType(StringUtils.capitalize(type));
    }

    /* example of old way of getting data by id ->
    public Amp getAmp(String id) {
        return amps.stream()
                .filter(guitar -> guitar.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
    */

    public void addHead(Head head) {
        ampRepository.save(head);
    }

    public void addCombo(Combo combo) {
        ampRepository.save(combo);
    }

    /* example of old way of saving data ->
    public void addAmp(Amp amp) {
        amps.add(amp);
    }*/
}
